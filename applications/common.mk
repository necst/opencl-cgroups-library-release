#NOTE: adjust includes and library paths of OpenCL

INCLUDE= -I ../../powermonitor-lib -I ../../opencl-cgroups-lib -I ../../common #-I${OpenCL_SDK}/OpenCL/common/inc 
LIBPATH= -L/home/odroid/Desktop/ocl-icd-2.2.7/.libs
LIB=-lOpenCL -lm -lcgroup
OBJ_FILES=../../powermonitor-lib/powermonitor-lib.o ../../opencl-cgroups-lib/openclController.o ../../common/utils.o ../../opencl-cgroups-lib/CGroupUtils.o
all:
	g++ -O3 -std=gnu++11 ${INCLUDE} ${OBJ_FILES} ${CFILES} ${LIBPATH} ${LIB} -o ${EXECUTABLE} -lrt

clean:
	rm -f *~ *.exe
