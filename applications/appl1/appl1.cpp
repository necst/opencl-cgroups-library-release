/*
****************************** Copyright & License *****************************
OpenCL-CGroups-Library v0.1 is a software controller for OpenCL applications running on
Heterogeneous System Architectures hosting a Linux operating system for optimizing
the performance/Watt metric. Copyright (C) 2016 Politecnico di Milano.

This framework is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This framework is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details 
(http://www.gnu.org/licenses/).

Neither the name of Politecnico di Milano nor the names of its contributors may be 
used to endorse or promote products derived from this software without specific 
prior written permission.
********************************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#include "../../common/polybenchUtilFuncts.h"
#include "../../opencl-cgroups-lib/openclController.h"


#define MAX_SOURCE_SIZE (0x100000)

/* Problem size */
#define N 4096

/* NDrange dimensions */
#define DIM_LOCAL_WORK_GROUP_X 32
#define DIM_LOCAL_WORK_GROUP_Y 8

/* Maximum number of contexts managed*/
#define MAX_CONTEXTS 2


/* OpenCL variables */
cl_platform_id platform_id[MAX_CONTEXTS];
cl_device_id device_id[MAX_CONTEXTS];   
cl_uint num_devices;
cl_uint num_platforms;
cl_int errcode;
cl_context clContext[MAX_CONTEXTS];
cl_kernel clKernel[MAX_CONTEXTS];
cl_command_queue clCommandQue[MAX_CONTEXTS];
cl_program clProgram[MAX_CONTEXTS];
cl_mem a_mem_obj[MAX_CONTEXTS];
cl_mem b_mem_obj[MAX_CONTEXTS];


/* Currently used device*/
int currDevice;

/*Prototypes*/
void init(int *A);
void cl_initialization();
void cl_launch_kernel(int* MA, int* MB);
void cl_clean_up();

void init(int *A) {
  int i;
  for (i = 0; i < N; i++)
    A[i] = rand();
}

void cl_initialization() {
  char str_temp[1024]; //used to save infos queried to the platforms
  int i;
  
  // Get platform and device information
  errcode = clGetPlatformIDs(0, NULL, &num_platforms);
  if(errcode == CL_SUCCESS) printf("number of platforms is %d\n",num_platforms);
  else printf("Error getting platform IDs\n");

  errcode = clGetPlatformIDs(num_platforms, platform_id, NULL);
  if(errcode != CL_SUCCESS) printf("Error getting platform IDs\n");

  //for each platform get the first device! (it is possible to generalize it to configure all devices)
  for(i=0; i<num_platforms; i++){
    errcode = clGetPlatformInfo(platform_id[i],CL_PLATFORM_NAME, sizeof(str_temp), str_temp,NULL);
    if(errcode == CL_SUCCESS) printf("platform name is %s\n",str_temp);
    else printf("Error getting platform name\n");

    errcode = clGetPlatformInfo(platform_id[i], CL_PLATFORM_VERSION, sizeof(str_temp), str_temp,NULL);
    if(errcode == CL_SUCCESS) printf("platform version is %s\n",str_temp);
    else printf("Error getting platform version\n");

    errcode = clGetDeviceIDs( platform_id[i], CL_DEVICE_TYPE_ALL, 1, &device_id[i], &num_devices);
    if(errcode == CL_SUCCESS) printf("number of devices is %d (we take the first one)\n", num_devices);
    else printf("Error getting device IDs\n");

    errcode = clGetDeviceInfo(device_id[i],CL_DEVICE_NAME, sizeof(str_temp), str_temp,NULL);
    if(errcode == CL_SUCCESS) printf("device name is %s\n",str_temp);
    else printf("Error getting device name\n");
  
    // Create an OpenCL context
    clContext[i] = clCreateContext( NULL, 1, &device_id[i], NULL, NULL, &errcode);
    if(errcode != CL_SUCCESS) printf("Error in creating context\n");
   
    //Create a command-queue
    clCommandQue[i] = clCreateCommandQueue(clContext[i], device_id[i], 0, &errcode);
    if(errcode != CL_SUCCESS) printf("Error in creating command queue\n");
  }
  
  //The above part is application-independent
  //Application specific settings follow...

  //For each configured device set up memory buffers for inputs and outputs
  for(i=0; i<num_platforms; i++){
    a_mem_obj[i] = clCreateBuffer(clContext[i], CL_MEM_READ_ONLY, sizeof(int) * N, NULL, &errcode);
    if(errcode != CL_SUCCESS) printf("Error in creating buffers\n");
    b_mem_obj[i] = clCreateBuffer(clContext[i], CL_MEM_READ_WRITE, sizeof(int) * N, NULL, &errcode);  
    if(errcode != CL_SUCCESS) printf("Error in creating buffers\n");
  }

  // Load the kernel source code into the array source_str
  FILE *fp;
  char *source_str;
  size_t source_size;
  fp = fopen("appl1.cl", "r");
  if (!fp) {
    fprintf(stdout, "Failed to load kernel.\n");
    exit(1);
  }
  source_str = (char*)malloc(MAX_SOURCE_SIZE);
  source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
  fclose(fp);

  //For each configured device build the program
  for(i=0; i<num_platforms; i++){
    // Create a program from the kernel source
    clProgram[i] = clCreateProgramWithSource(clContext[i], 1, (const char **)&source_str, (const size_t *)&source_size, &errcode);

    if(errcode != CL_SUCCESS) printf("Error in creating program\n");

    // Build the program
    errcode = clBuildProgram(clProgram[i], 1, &device_id[i], NULL, NULL, NULL);
    if(errcode != CL_SUCCESS) printf("Error in building program\n");
    
    // Create the OpenCL kernel
    clKernel[i] = clCreateKernel(clProgram[i], "appl1_kernel", &errcode);
    if(errcode != CL_SUCCESS) printf("Error in creating kernel\n");
    //clFinish(clCommandQue[i]);
  }
  free(source_str);
}

void cl_launch_kernel(int* MA, int* MB)
{
  double t_start, t_end;
  int n = N;

  size_t localWorkSize, globalWorkSize;
  localWorkSize = DIM_LOCAL_WORK_GROUP_X;
  globalWorkSize = (size_t)ceil(((float)N) / ((float)DIM_LOCAL_WORK_GROUP_X)) * DIM_LOCAL_WORK_GROUP_X;

  t_start = rtclock();

  errcode = clEnqueueWriteBuffer(clCommandQue[currDevice], a_mem_obj[currDevice], CL_TRUE, 0, sizeof(int) * N, MA, 0, NULL, NULL);
  if(errcode != CL_SUCCESS)printf("Error in writing buffers\n");

  
  // Set the arguments of the kernel
  errcode =  clSetKernelArg(clKernel[currDevice], 0, sizeof(cl_mem), (void *)&a_mem_obj[currDevice]);
  errcode |= clSetKernelArg(clKernel[currDevice], 1, sizeof(cl_mem), (void *)&b_mem_obj[currDevice]);
  errcode |=  clSetKernelArg(clKernel[currDevice], 2, sizeof(int), &n);
  
  if(errcode != CL_SUCCESS) printf("Error in seting arguments\n");
  // Execute the OpenCL kernel
  errcode = clEnqueueNDRangeKernel(clCommandQue[currDevice], clKernel[currDevice], 1, NULL, &globalWorkSize, &localWorkSize, 0, NULL, NULL);

  if(errcode != CL_SUCCESS) printf("Error in launching kernel\n");
  clFinish(clCommandQue[currDevice]);

  errcode = clEnqueueReadBuffer(clCommandQue[currDevice], b_mem_obj[currDevice], CL_TRUE, 0, N*sizeof(int), MB, 0, NULL, NULL);
  if(errcode != CL_SUCCESS) printf("Error in reading GPU mem\n");

  t_end = rtclock();
  fprintf(stdout, "HB\t%0.6lfs\t%0.6lfs\n", t_end - t_start, t_end);
}

void cl_clean_up()
{
  // Clean up
  int i;
  for(i=0; i<num_platforms; i++){
    errcode = clReleaseKernel(clKernel[i]);
    errcode |= clReleaseProgram(clProgram[i]);
    errcode |= clReleaseMemObject(a_mem_obj[i]);
    errcode |= clReleaseMemObject(b_mem_obj[i]);
    errcode |= clReleaseCommandQueue(clCommandQue[i]);
    errcode |= clReleaseContext(clContext[i]);
    if(errcode != CL_SUCCESS) printf("Error in cleanup\n");
  }
}

int main(int argc, char *argv[])
{
  int i, iters, tmp_device;
  int A[N];
  int B[N]; 

  //initialize to run on the first device
  currDevice = 0;
  
  //get command line arguments if any
  if(argc==1){ 
    currDevice = 0;
    iters = 1;
  } else if(argc==2){
    iters = atoi(argv[1]);
  } else {
    printf("USAGE: %s [ITERATIONS]",argv[0]);
    exit(0);
  }

  //setup input data
  init(A);

  //initialize OpenCL
  cl_initialization();

  //initialize controller
  OpenClController controller;
  int oldFreq = 0;

  //main loop
  for(i = 0; i<iters; i++){
    currDevice = controller.startKernel();
    controller.startTimer();
    std::cout << "Device: " << controller.currentConfiguration.d->type << " @ " << controller.getConfigurationFrequency() << " MHz" << std::endl;
    cl_launch_kernel(A,B);
    controller.stopKernel();
  }

  //clean system
  cl_clean_up();

  return 0;
}

