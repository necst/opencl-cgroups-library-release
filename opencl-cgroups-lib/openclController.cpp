/*
****************************** Copyright & License *****************************
OpenCL-CGroups-Library v0.1 is a software controller for OpenCL applications running on
Heterogeneous System Architectures hosting a Linux operating system for optimizing
the performance/Watt metric. Copyright (C) 2016 Politecnico di Milano.

This framework is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This framework is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details 
(http://www.gnu.org/licenses/).

Neither the name of Politecnico di Milano nor the names of its contributors may be 
used to endorse or promote products derived from this software without specific 
prior written permission.
********************************************************************************
*/


#include "openclController.h"
#include "utils.h"
#include <vector>
#include <numeric>
#include <assert.h>
#include <math.h>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>

#include "CGroupUtils.h"

//#include <Eigen/Dense>

#define ATTEMPS 1

void ParabolaEstimator::addPoint(float x, float y){
    this->points.push_back(std::pair<float, float> (x, y));
}

void ParabolaEstimator::getMax(float &x, float &y){
    x = this->xMax;
    y = this->yMax;
}

void ParabolaEstimator::estimate(){
    // TODO: Improve points selection

//    float x1 = points[0].first;
//    float x2 = points[1].first;
//    float x3 = points[2].first;

//    float y1 = points[0].second;
//    float y2 = points[1].second;
//    float y3 = points[2].second;

//    Eigen::Matrix3f A;
//    Eigen::Vector3f terms;

//    A << x1*x1,x1,1, x2*x2,x2,1, x3*x3,x3,1;
//    terms << y1, y2, y3;
//    Eigen::Vector3f sol = A.colPivHouseholderQr().solve(terms);

//    a = ( y2 * (x3 - x1) - y1 * (x3 - x2) - y3 * (x2 - x1) ) / ( x1*x1 * ( x2 - x3 ) - x3*x3 * ( x2 - x1 ) - x2*x2 * (x1 - x3) );
//    b = ( y2 - y1 + a * (x1*x1 - x2*x2) ) / (x2 - x1);
//    c = -a * x2*x2 - b * x1 + y1;

//    xMax = -b/(2 * a);
//    yMax = (- b*b + 4 *a * c) / ( 4 * a);

//    std::cout << "ESTIMATION" << std::endl;
//    std::cout << y1 << "\t" << y2 << "\t" << y3 << std::endl;
//    std::cout << a << "\t" << b << "\t" << c << std::endl;
//    std::cout << sol << std::endl;

//    std::cout << "a: " << a << "\tb: " << b << "\tc: " << c << std::endl;
//    std::cout << sol << std::endl;
//    std::cout << "Here is the matrix A:\n" << A << std::endl;
//    std::cout << "Here is the vector b:\n" << terms << std::endl;
}

void Device::addPeformancePoint(float frequency, float efficiency){
    this->frequencies2performance[frequency].push_back(efficiency);

    estimator.addPoint(frequency, efficiency);
}

float Device::getEstimationAtFrequency(float frequency){
    assert(this->frequencies2performance.find(frequency) != this->frequencies2performance.end());
    return this->frequencies2performance[frequency][this->frequencies2performance[frequency].size()-1];
}

Device::Device(std::string type, std::vector<float> frequencies){
    training = true;
    this->frequencies = frequencies;
    lowFrequencyBound = frequencies[0];
    highFrequencyBound = frequencies[frequencies.size() - 1];
    maxEstimation = frequencies[frequencies.size() / 2];
    this->type = type;
    converged = false;
}

bool Device::hasConverged(){
    return this->converged;
}

void Device::setFrequency(float frequency){
    this->currentFrequency = frequency;
}

CpuDevice::CpuDevice(std::string type, std::vector<unsigned int> processors, std::vector<float> frequencies) : Device(type, frequencies){
    this->processors = processors;
}

void CpuDevice::setFrequency(float frequency){
    Device::setFrequency(frequency);

    // cpufrequtils
//    std::cout << "CPU Set Frequency " << this->type << " @ " << frequency << std::endl;

    // CPU Set
    std::vector<int> proc;
    for(auto p: processors)
        proc.push_back((int) p);
    CGUtils::UpdateCpuSet(getpid(), proc);
//    std::cout << "Constraining on cluster with " << processors[0] << std::endl;

    char cmd[200];

    int freq = frequency;

    sprintf(cmd, "cpufreq-set -c %d --min %dMHz --max %dMHz", processors[0], freq, freq);
//    std::cout << cmd << std::endl;
    int ret = system(cmd);

    sprintf(cmd, "cpufreq-set -c %d --min %dMHz --max %dMHz", 4-processors[0], 200, 200);
//    std::cout << cmd << std::endl;
    ret = system(cmd);
}

void GpuDevice::setFrequency(float frequency){
    Device::setFrequency(frequency);

    // write in /proc/fs
//    std::cout << "GPU Set Frequency " << this->type << " @ " << frequency << std::endl;
    int freq = frequency;
    char cmd[200];

    sprintf(cmd, "echo %d > /sys/devices/11800000.mali/dvfs_min_lock", freq);
    int ret = system(cmd);

    sprintf(cmd, "echo %d > /sys/devices/11800000.mali/dvfs_max_lock", freq);
    ret = system(cmd);

    sprintf(cmd, "cpufreq-set -c %d --min %dMHz --max %dMHz", 0, 1300, 1300);
//    std::cout << cmd << std::endl;
    ret = system(cmd);

    sprintf(cmd, "cpufreq-set -c %d --min %dMHz --max %dMHz", 4, 200, 200);
//    std::cout << cmd << std::endl;
    ret = system(cmd);

    std::vector<int> big;
    big.push_back(0);
    big.push_back(1);
    big.push_back(2);
    big.push_back(3);

    CGUtils::UpdateCpuSet(getpid(), big);
}

GpuDevice::GpuDevice(std::string type, std::vector<float> frequencies) : Device(type, frequencies){}

unsigned int ParabolaEstimator::getNumberOfSamples(){
    return this->points.size();
}


Configuration Device::getNextConfiguration(){

    int dataPoints = frequencies2performance.size();
//    int dataPoints = this->estimator.getNumberOfSamples();
    float nextFrequency = currentFrequency;

//    std::cout << this->type << " : " << dataPoints << std::endl;

    switch(dataPoints){
        case 0:
            // Return min freq Configuration
            nextFrequency = lowFrequencyBound;
            break;
        case 1:
            // Return max freq Configuration
            nextFrequency = highFrequencyBound;
            break;
        case 2:
            // Return middle freq Configuration
            nextFrequency = frequencies[frequencies.size()/2];
            maxEstimation = nextFrequency;
            training = false;
            break;
        default:

//            estimator.estimate();
//            float maxFrequency;
//            float maxEfficiency;

//            estimator.getMax(maxFrequency, maxEfficiency);

//            float minDiff = frequencies[0];
//            for(auto f: frequencies){
//                if( fabs(f - maxFrequency) < fabs(minDiff - maxFrequency))
//                    minDiff = f;
//            }

//            nextFrequency = minDiff;

            // Check that correct frequencies are used, otherwise crash badly
            assert(std::find(frequencies.begin(), frequencies.end(), lowFrequencyBound) != frequencies.end());
            assert(std::find(frequencies.begin(), frequencies.end(), highFrequencyBound) != frequencies.end());
            assert(std::find(frequencies.begin(), frequencies.end(), currentFrequency) != frequencies.end());
            assert(std::find(frequencies.begin(), frequencies.end(), maxEstimation) != frequencies.end());

            // Check that estimation for interesting points exist, otherwise crash badly
            assert(frequencies2performance.find(lowFrequencyBound) != frequencies2performance.end());
            assert(frequencies2performance.find(highFrequencyBound) != frequencies2performance.end());
            assert(frequencies2performance.find(currentFrequency) != frequencies2performance.end());
            assert(frequencies2performance.find(maxEstimation) != frequencies2performance.end());


            // Create points in the performance / frequency plot
            std::vector<float> freqPoints;
            freqPoints.push_back(lowFrequencyBound);
            freqPoints.push_back(maxEstimation);
            freqPoints.push_back(highFrequencyBound);
            freqPoints.push_back(currentFrequency);

            std::vector<std::pair<float, float>> points;

            for(auto p: freqPoints){
                float yp = std::accumulate(frequencies2performance[p].begin(), frequencies2performance[p].end(), 0.0) / frequencies2performance[p].size();
                points.push_back(std::pair<float, float>(p, yp));
            }

            // If current current frequency lead to better performance, then it is a good estimation of the max performance
            // and I can reduce the interval
            if(points[3].second >= points[1].second){
                maxEstimation = currentFrequency;
            }

            std::sort(freqPoints.begin(), freqPoints.end());
            unsigned int maxPosition = std::find(freqPoints.begin(), freqPoints.end(), maxEstimation) - freqPoints.begin();

            // Max should be in the middle, otherwise crash badly
            assert(maxPosition!=0);
            assert(maxPosition!=freqPoints.size() - 1);

            lowFrequencyBound = freqPoints[maxPosition - 1];
            highFrequencyBound = freqPoints[maxPosition + 1];

            unsigned int frequencyIndexLow = std::find(frequencies.begin(), frequencies.end(), lowFrequencyBound) - frequencies.begin();
            unsigned int frequencyIndexHigh = std::find(frequencies.begin(), frequencies.end(), highFrequencyBound) - frequencies.begin();
            unsigned int frequencyIndexMax = std::find(frequencies.begin(), frequencies.end(), maxEstimation) - frequencies.begin();

            unsigned int newLowPos = (frequencyIndexLow + frequencyIndexMax) / 2;
            unsigned int newHighPos = (frequencyIndexHigh + frequencyIndexMax) / 2;

//            std::cout << "old: [ " << frequencies[frequencyIndexLow] << " , " << frequencies[frequencyIndexMax] << " ] : " << frequencies[frequencyIndexMax] << std::endl;
//            std::cout << "new: [ " << frequencies[newLowPos] << " , " << frequencies[newHighPos] << " ] : " << frequencies[frequencyIndexMax] << std::endl;

            // Constrain lower bound
            if(newLowPos != frequencyIndexLow){
//                std::cout << "NOT Converged" << std::endl;
                nextFrequency = frequencies[newLowPos];
            }
            // Constrain upper bound
            else if(newHighPos != frequencyIndexHigh){
//                std::cout << "NOT Converged" << std::endl;
                nextFrequency = frequencies[newHighPos];
            }
            // Converged to one value
            else{
//                std::cout << "Converged" << std::endl;
                nextFrequency = frequencies[frequencyIndexMax];
                converged = true;
            }

            break;
    }

    Configuration c;
    c.valid = true;
    c.d = this;
    c.frequency = nextFrequency;

    return c;
}


OpenClController::OpenClController(){
    powermon = new PowerMonitor(CLIENT_MODE);
    attempsAtCurrentFrequency = 0;

    // Initialize Devices
    std::vector<unsigned int> bigCores;
    std::vector<unsigned int> littleCores;

    littleCores.push_back(0);
    littleCores.push_back(1);
    littleCores.push_back(2);
    littleCores.push_back(3);
    bigCores.push_back(4);
    bigCores.push_back(5);
    bigCores.push_back(6);
    bigCores.push_back(7);

    std::vector<float> littleFrequencies;
    std::vector<float> bigFrequencies;
    std::vector<float> gpuFrequencies;

    for(int i=200; i<=1300; i+=100)
        littleFrequencies.push_back((float) i);

    for(int i=200; i<=1900; i+=100)
        bigFrequencies.push_back((float) i);

    // TODO: Check Frequencies
    gpuFrequencies.push_back(266);
    gpuFrequencies.push_back(350);
    gpuFrequencies.push_back(420);
    gpuFrequencies.push_back(480);
    gpuFrequencies.push_back(543);
    gpuFrequencies.push_back(600);

    CpuDevice *bigCluster = new CpuDevice("BIG", bigCores, bigFrequencies);
    CpuDevice *littleCluster = new CpuDevice("LITTLE", littleCores, littleFrequencies);
    GpuDevice *mali = new GpuDevice("MALI", gpuFrequencies);

    devicesList.push_back(bigCluster);
    devicesList.push_back(littleCluster);
    devicesList.push_back(mali);

    CGUtils::Setup();
    CGUtils::Initialize(getpid());

    std::cout << "Big: " << bigCluster << std::endl;
    std::cout << "Little: " << littleCluster << std::endl;
    std::cout << "Mali: " << mali << std::endl;
}

Configuration OpenClController::getTrainingConfiguration(){
    for(auto &d : devicesList){
        if(d->training)
            return d->getNextConfiguration();
    }

    Configuration c;
    c.valid = false;
    return c;
}

Configuration OpenClController::getConfiguration(){
//    Configuration c = this->getTrainingConfiguration();

//    if(c.valid==true)
//       return c;

    for(auto d : devicesList){
        if(!d->hasConverged())
            return d->getNextConfiguration();
    }

    std::vector<std::pair<float, int>> bestEfficiency;

    for(int i=0; i<devicesList.size(); i++){
        float xMax, yMax;
        xMax = devicesList[i]->maxEstimation;
        yMax = devicesList[i]->getEstimationAtFrequency(xMax);
//        devicesList[i]->estimator.estimate();
//        devicesList[i]->estimator.getMax(xMax, yMax);
        bestEfficiency.push_back( std::pair<float, int>(yMax, i) );
//        std::cout << "Device: " << i << "\t" << "efficiency: " << yMax << " @ " << xMax << std::endl;
    }

    std::sort(bestEfficiency.begin(), bestEfficiency.end());
    std::pair<float, int> bestDevice = bestEfficiency[bestEfficiency.size()-1];

    std::cout << "Chosen " << bestDevice.second << "\t" << bestDevice.first << std::endl;
    this->currentConfiguration.d = devicesList[bestDevice.second];

//    std::cout << "Best conf: " << this->currentConfiguration.d << std::endl;
    return this->currentConfiguration.d->getNextConfiguration();
}

unsigned long OpenClController::startTimer(){
    this->startTime = getTime();
}

unsigned int OpenClController::startKernel(){

    if(attempsAtCurrentFrequency == ATTEMPS){
        attempsAtCurrentFrequency = 0;
    }

//    std::cout << "Getting Configuration" << std::endl;
    if(attempsAtCurrentFrequency == 0)
        this->currentConfiguration = getConfiguration();

    attempsAtCurrentFrequency++;

//    std::cout << "Setting Frequency" << std::endl;
    this->currentConfiguration.d->setFrequency(this->currentConfiguration.frequency);

    unsigned int deviceId = std::find(devicesList.begin(), devicesList.end(), this->currentConfiguration.d) - devicesList.begin();

//    std::cout << "Starting power acquisition" << std::endl;
    this->powermon->startAcquisition();

//    std::cout << "Executing on " << deviceId << std::endl;

    return (deviceId!=2); // deviceId 2 is GPU which is mapped as 0 by OpenCl
    // TODO: This should be refactored
}

void OpenClController::stopKernel(){
    this->stopTime = getTime();

    unsigned long totalTime = this->stopTime - this->startTime;

    float throughput = 1 / (totalTime * 1.0 / 1000 / 1000);

    this->powermon->stopAcquisition();
    this->powermon->acquireData();

    float power = this->powermon->getTotalPower();

    float efficiency = throughput/power;

//    std::cout << this->currentConfiguration.d->type << " @ " << this->currentConfiguration.frequency << " = " << efficiency << std::endl;

    this->currentConfiguration.d->addPeformancePoint(this->currentConfiguration.frequency, efficiency);

    std::cout << "REPORT\t" << this->startTime << "\t" << this->stopTime << "\t" << this->currentConfiguration.d->type << "\t" <<  this->currentConfiguration.frequency << "\t" << power << "\t" << throughput << "\t" << efficiency << std::endl;
}

float OpenClController::getConfigurationFrequency(){
    return this->currentConfiguration.frequency;
}
