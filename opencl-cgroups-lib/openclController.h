/*
****************************** Copyright & License *****************************
OpenCL-CGroups-Library v0.1 is a software controller for OpenCL applications running on
Heterogeneous System Architectures hosting a Linux operating system for optimizing
the performance/Watt metric. Copyright (C) 2016 Politecnico di Milano.

This framework is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This framework is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details 
(http://www.gnu.org/licenses/).

Neither the name of Politecnico di Milano nor the names of its contributors may be 
used to endorse or promote products derived from this software without specific 
prior written permission.
********************************************************************************
*/


#ifndef OPENCL_CONTROLLER
#define OPENCL_CONTROLLER

#include <string>
#include <vector>
#include <map>
#include "powermonitor-lib.h"

class ParabolaEstimator{
private:
    float a;
    float b;
    float c;

    std::vector<std::pair<float, float>> points;

    float xMax;
    float yMax;

public:
    // Add a point to the estimator
    void addPoint(float x, float y);

    // Return estimation of the max
    void getMax(float &x, float &y);

    // Estimate parameters
    void estimate();

    // Number of available points
    unsigned int getNumberOfSamples();

};

class Device{
private:
    std::vector<float> frequencies;
    std::map<float, std::vector<float>> frequencies2performance;


    float lowFrequencyBound;

    float highFrequencyBound;
    float currentFrequency;

    bool converged;

public:
    Device(std::string type, std::vector<float> frequencies);

    std::string type;

    ParabolaEstimator estimator;
    float maxEstimation;

    bool hasConverged();

    virtual void setFrequency(float frequency) = 0;
    void addPeformancePoint(float frequency, float efficiency);
    struct Configuration getNextConfiguration();
    float getEstimationAtFrequency(float frequency);


    bool training;
};

typedef struct Configuration{
    Device *d;
    float frequency;
    double valid;
} Configuration;


class CpuDevice : public Device{
private:
    std::vector<unsigned int> processors;

public:
    CpuDevice(std::string type, std::vector<unsigned int> processors, std::vector<float> frequencies);
    void setFrequency(float frequency);

};

class GpuDevice : public Device{
public:
    GpuDevice(std::string type, std::vector<float> frequencies);
    void setFrequency(float frequency);
};

class OpenClController{

private:
    PowerMonitor * powermon;

    unsigned long startTime;
    unsigned long stopTime;

    std::vector<Device*> devicesList;

    Configuration getTrainingConfiguration();
    Configuration getConfiguration();

    unsigned int attempsAtCurrentFrequency;

public:
    OpenClController();

    Configuration currentConfiguration;

    unsigned int startKernel();
    void stopKernel();

    unsigned long startTimer();

    float getConfigurationFrequency();
};

#endif
