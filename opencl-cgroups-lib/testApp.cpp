/*
****************************** Copyright & License *****************************
OpenCL-CGroups-Library v0.1 is a software controller for OpenCL applications running on
Heterogeneous System Architectures hosting a Linux operating system for optimizing
the performance/Watt metric. Copyright (C) 2016 Politecnico di Milano.

This framework is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This framework is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details 
(http://www.gnu.org/licenses/).

Neither the name of Politecnico di Milano nor the names of its contributors may be 
used to endorse or promote products derived from this software without specific 
prior written permission.
********************************************************************************
*/


#include "openclController.h"
#include <unistd.h>
#include <iostream>
#include <math.h>

void kernel(unsigned int deviceId, float frequency){
    float sleepTime = 0.0;

    // Simulate sleep time
    float a = -0.125;
    float b;

    std::cout << "Device Id: " << deviceId << "\tFrequency: " << frequency << std::endl;

//    e^(- ( (x)/100) + 10) + 50


    switch(deviceId){
        case 0:
            b = 50;
            break;
        case 1:
            b = 80;
            break;
        case 2:
            b = 30;
            break;
    }

    sleepTime = exp( - (frequency/100) + 10 ) + b;
    sleepTime *= 1000;

//    printf("sleeping for: %f\n", sleepTime);

    usleep(sleepTime);
}

int main(int argc, char **argv){

    if(argc!=2){
        std::cout << "USAGE: ./testApp ITERATIONS" << std::endl;
        exit(0);
    }

    int iterations = atoi(argv[1]);

    OpenClController controller;

    for(int i = 0; i<iterations; i++){
        unsigned int deviceId = controller.startKernel();
        kernel(deviceId, controller.getConfigurationFrequency());
        controller.stopKernel();
    }

}
