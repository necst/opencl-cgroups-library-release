/*
****************************** Copyright & License *****************************
OpenCL-CGroups-Library v0.1 is a software controller for OpenCL applications running on
Heterogeneous System Architectures hosting a Linux operating system for optimizing
the performance/Watt metric. Copyright (C) 2016 Politecnico di Milano.

This framework is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This framework is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details 
(http://www.gnu.org/licenses/).

Neither the name of Politecnico di Milano nor the names of its contributors may be 
used to endorse or promote products derived from this software without specific 
prior written permission.
********************************************************************************
*/


#ifndef UTILS_CGROUPS_H_
#define UTILS_CGROUPS_H_

#include <algorithm>
#include <vector>
#include <sys/types.h>

namespace CGUtils {

enum CGControllers {
	CPUSET = 0,
	CPU,
	FREEZER,
 	CGCONTROLLERS_COUNT
};

/// Checks if the root control group is mounted on the file system.
/// Initialize libCGroup internal structures.
/// Enables control groups support in Orchestrator on success.
/// ALERT: To be called only once.
/// @return 0 on success; -1 on failure
int Setup();

/// Create a CGroup and attach the application appID to it
/// @return appID on success; -1 on failure.
int Initialize(pid_t appID);

/// Removes the CGroup associated to the application appID.
/// @return appID on success; -1 on failure.
int Remove(pid_t appID);

/// Writes the CPUset represented by the vector 'cores' into the CGroup
/// associated to the application appID.
/// @return appID on success; -1 on failure.
int UpdateCpuSet(pid_t appID, std::vector<int> cores);

/// Writes the CPU quota represented by 'quota' into the CGroup associated to
/// the application appID.
/// @param quota: X CPUS => X,0 (e.g 1 CPU and a half => 1,5)
/// @return appID on success; -1 on failure.
int UpdateCpuQuota(pid_t appID, float quota);

/// Set the freeze status of the CGroup associated to
/// the application appID according to the 'freeze' boolean parameter.
/// @return appID on success; -1 on failure.
int UpdateCpuFreezer(pid_t appID, bool freeze);

} // end namespace

#endif // UTILS_CGROUPS_H_
