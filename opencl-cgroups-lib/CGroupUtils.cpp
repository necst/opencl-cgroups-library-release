/*
****************************** Copyright & License *****************************
OpenCL-CGroups-Library v0.1 is a software controller for OpenCL applications running on
Heterogeneous System Architectures hosting a Linux operating system for optimizing
the performance/Watt metric. Copyright (C) 2016 Politecnico di Milano.

This framework is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This framework is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details 
(http://www.gnu.org/licenses/).

Neither the name of Politecnico di Milano nor the names of its contributors may be 
used to endorse or promote products derived from this software without specific 
prior written permission.
********************************************************************************
*/

#include "CGroupUtils.h"
#include <iostream>
#include <libcgroup.h>
#include <vector>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

//on ubuntu >=15.04 cgroups work only within this folder... 
//on different machine we can set the desired folder name (before we have to run the bash script to configure
#define MAINGROUP "/user.slice/" 

// using namespace std;

namespace CGUtils {

// NOTE: this should match the CGControllers enum
const char *controller[] = {
    "cpuset",
    "cpu",
    "freezer",
};

// Needed controllers
std::vector<int> controller_ids = {
    CPUSET,
    CPU,
    FREEZER,
};

char *mounts[CGCONTROLLERS_COUNT] = { nullptr, };
const unsigned int cfs_period = 100000;

// To be called ONLY once when the orchestrator starts. It setups libcgroup
// internal info and searches for mount points.
int Setup() {
    int result = 0;
    struct cgroup *p_cg;

    // Initialization
    result = cgroup_init();
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
        return -1;
    }

    // Looking for controllers
    for (int id : controller_ids) {
        result = cgroup_get_subsys_mount_point(controller[id], &mounts[id]);
        if (result) {
            std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
            return -1;
        } else {
            std::cout << "CGroup controller [" << controller[id]
                      << "] available at [" << mounts[id]<< "]" << std::endl;
        }
    }
    return 0;
}

int Initialize(pid_t appID) {
    int result = 0;
    std::string cg_mount_point = "";
    struct cgroup *p_cg;
    struct cgroup_controller *p_cg_contr_cpuset;
    struct cgroup_controller *p_cg_contr_cpu;
    struct cgroup_controller *p_cg_contr_freezer;

    // Creating a sample CGroup with the app PID
    std::string cg_name = std::to_string(appID);
    std::string app_mount_point = MAINGROUP + cg_name;

    p_cg = cgroup_new_cgroup(app_mount_point.c_str());
    if (!p_cg) {
        std::cerr << "Error: cannot create " << app_mount_point << std::endl;
        return -1;
    }

    // Set CPUSET configuration (if available)
    if (!mounts[CPUSET]) {
        std::cerr << app_mount_point << ": CPUSET controller not mounted"
                  << std::endl;
            return -1;
    }

    p_cg_contr_cpuset = cgroup_add_controller(p_cg, "cpuset");
    if (!p_cg_contr_cpuset) {
        std::cerr << app_mount_point << ": set CPUSET controller FAILED"
                  << std::endl;
        return -1;
    }

    result = cgroup_set_value_string(p_cg_contr_cpuset, "cpuset.cpus", "0-3");
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
        return -1;
    }

    result = cgroup_set_value_string(p_cg_contr_cpuset, "cpuset.mems", "0");
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
        return -1;
    }

    // Set CPU configuration
    if (!mounts[CPU]) {
        std::cerr << app_mount_point << ": CPU controller not configured"
                  << std::endl;
        return -1;
    }

    p_cg_contr_cpu = cgroup_add_controller(p_cg, "cpu");
    if (!p_cg_contr_cpu) {
        std::cerr << app_mount_point << ": set CPU controller FAILED"
                  << std::endl;
        return -1;
    }

    result = cgroup_set_value_string(p_cg_contr_cpu, "cpu.cfs_period_us",
                std::to_string(cfs_period).c_str());
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
        return -1;
    }

    result = cgroup_set_value_string(p_cg_contr_cpu, "cpu.cfs_quota_us", "-1");
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
        return -1;
    }
    
    //Set freezer configuration
    if (!mounts[FREEZER]) {
        std::cerr << app_mount_point << ": FREEZER controller not mounted"
                  << std::endl;
            return -1;
    }

    p_cg_contr_freezer = cgroup_add_controller(p_cg, "freezer");
    if (!p_cg_contr_freezer) {
        std::cerr << app_mount_point << ": set FREEZER controller FAILED"
                  << std::endl;
        return -1;
    }
    
    result = cgroup_set_value_string(p_cg_contr_freezer, "freezer.state", "THAWED");
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
        return -1;
    }

    // Create the kernel-space CGroup
    result = cgroup_create_cgroup(p_cg, 1);
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
        return -1;
    }

    // Setting app into the CGroup
    result = cgroup_attach_task_pid (p_cg, appID);
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
    }

    result = cgroup_modify_cgroup(p_cg);
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
    }

    cgroup_free(&p_cg);
    return appID;
}

int Remove(pid_t appID) {
    int result = 0;
    struct cgroup *p_cg;

    // Creating a sample CGroup with the app PID
    std::string cg_name = std::to_string(appID);
    std::string app_mount_point = MAINGROUP + cg_name;

    p_cg = cgroup_new_cgroup(app_mount_point.c_str());
    if (!p_cg) {
        std::cerr << "Error: cannot retrieve " << app_mount_point << std::endl;
        return -1;
    }

    result = cgroup_delete_cgroup(p_cg, 1);
    cgroup_free(&p_cg);

    return appID;
}

int UpdateCpuSet(pid_t appID, std::vector<int> cores) {
    int result = 0;
    struct cgroup *p_cg;
    struct cgroup_controller *p_cg_contr_cpuset;

    // Creating a sample CGroup with the app PID
    std::string cg_name = std::to_string(appID);
    std::string app_mount_point = MAINGROUP + cg_name;

    p_cg = cgroup_new_cgroup(app_mount_point.c_str());
    if (!p_cg) {
        std::cerr << "Error: cannot retrieve " << app_mount_point << std::endl;
        return -1;
    }

    p_cg_contr_cpuset = cgroup_add_controller(p_cg, "cpuset");
    if (!p_cg_contr_cpuset) {
        std::cerr << app_mount_point << ": retrieve CPUSET controller FAILED"
                  << std::endl;
        return -1;
    }

    if (cores.size() == 0) {
        std::cout << "Error: empty cpuset from policy" << std::endl;
        return -1;
    }

    std::string cpuset = std::to_string(cores[0]);

    for (int id = 1; id < cores.size(); id++) {
        cpuset += "," + std::to_string(cores[id]);
    }

    result = cgroup_set_value_string(p_cg_contr_cpuset, "cpuset.cpus",
                cpuset.c_str());
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
        return -1;
    }

    result = cgroup_modify_cgroup(p_cg);
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
    }

    cgroup_free(&p_cg);
    return appID;

}

// @param quota: X CPUS => X,0 (e.g 1 CPU and a half => 1,5)
int UpdateCpuQuota(pid_t appID, float quota) {

    int result = 0;
    struct cgroup *p_cg;
    struct cgroup_controller *p_cg_contr_cpu;

    // Creating a sample CGroup with the app PID
    std::string cg_name = std::to_string(appID);
    std::string app_mount_point = MAINGROUP + cg_name;

    p_cg = cgroup_new_cgroup(app_mount_point.c_str());
    if (!p_cg) {
        std::cerr << "Error: cannot retrieve " << app_mount_point << std::endl;
        return -1;
    }

    p_cg_contr_cpu = cgroup_add_controller(p_cg, "cpu");
    if (!p_cg_contr_cpu) {
        std::cerr << app_mount_point << ": set CPU controller FAILED"
                  << std::endl;
        return -1;
    }

    result = cgroup_set_value_string(p_cg_contr_cpu, "cpu.cfs_period_us",
                std::to_string(cfs_period).c_str());
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
        return -1;
    }

    result = cgroup_set_value_string(p_cg_contr_cpu, "cpu.cfs_quota_us",
        std::to_string((int)(quota * cfs_period)).c_str());
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
        return -1;
    }

    result = cgroup_modify_cgroup(p_cg);
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
    }

    cgroup_free(&p_cg);
    return appID;
}

int UpdateCpuFreezer(pid_t appID, bool freeze) {

    int result = 0;
    struct cgroup *p_cg;
    struct cgroup_controller *p_cg_contr_freezer;

    // Creating a sample CGroup with the app PID
    std::string cg_name = std::to_string(appID);
    std::string app_mount_point = MAINGROUP + cg_name;

    p_cg = cgroup_new_cgroup(app_mount_point.c_str());
    if (!p_cg) {
        std::cerr << "Error: cannot retrieve " << app_mount_point << std::endl;
        return -1;
    }

    p_cg_contr_freezer = cgroup_add_controller(p_cg, "freezer");
    if (!p_cg_contr_freezer) {
        std::cerr << app_mount_point << ": set CPU controller FAILED"
                  << std::endl;
        return -1;
    }

    if(freeze)
        result = cgroup_set_value_string(p_cg_contr_freezer, "freezer.state", "FROZEN");
    else
        result = cgroup_set_value_string(p_cg_contr_freezer, "freezer.state", "THAWED");
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
        return -1;
    }

    result = cgroup_modify_cgroup(p_cg);
    if (result) {
        std::cerr << "Error: " << cgroup_strerror(result) << std::endl;
    }

    cgroup_free(&p_cg);
    return appID;
}


}; // end CGUtils namespace

