/*
****************************** Copyright & License *****************************
OpenCL-CGroups-Library v0.1 is a software controller for OpenCL applications running on
Heterogeneous System Architectures hosting a Linux operating system for optimizing
the performance/Watt metric. Copyright (C) 2016 Politecnico di Milano.

This framework is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This framework is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details 
(http://www.gnu.org/licenses/).

Neither the name of Politecnico di Milano nor the names of its contributors may be 
used to endorse or promote products derived from this software without specific 
prior written permission.
********************************************************************************
*/

#ifndef UTILS
#define UTILS

#include <string>

#define SHARED_MEM_PATH "/tmp/"
#define	FILE_MODE	(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

typedef struct Report{
    unsigned long startTime;
    unsigned long endTime;
    unsigned int resource;
    double avg;
    double goal;
} Report;

unsigned long getTime();

void error_exit(std::string msg);

char *generateKernelId(std::string kernelName);

void * shm_mmap(std::string name, unsigned int size);

void createFifo(std::string name, int *inputFd, int *outputFd);
void destroyFifo(std::string name, int in, int out);

void terminalMessage(std::string msg, int type);

std::string int_to_str(int v);

#endif
