/*
****************************** Copyright & License *****************************
OpenCL-CGroups-Library v0.1 is a software controller for OpenCL applications running on
Heterogeneous System Architectures hosting a Linux operating system for optimizing
the performance/Watt metric. Copyright (C) 2016 Politecnico di Milano.

This framework is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This framework is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details 
(http://www.gnu.org/licenses/).

Neither the name of Politecnico di Milano nor the names of its contributors may be 
used to endorse or promote products derived from this software without specific 
prior written permission.
********************************************************************************
*/

#include "utils.h"

#include <errno.h>
#include <sys/time.h>
#include <sstream>
#include <ostream>
#include <assert.h>
#include <iostream>

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <fcntl.h>

unsigned long getTime(){
    struct timeval t;
    gettimeofday(&t, NULL);

    return t.tv_sec * 1000 * 1000 + t.tv_usec;
}

void error_exit(std::string msg){
    std::cout << msg << std::endl;
	exit(0);
}

void * shm_mmap(std::string name, unsigned int size){

//    printf("Shm mmap 0\n");

    std::string path = SHARED_MEM_PATH + name;

//    printf("Shm mmap 1\n");

    int fd=shm_open(path.c_str(), O_RDWR | O_CREAT, FILE_MODE);

//    perror("SHM_OPEN");

    assert(fd>0);

    ftruncate(fd,size);

    void *buffer=mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    close(fd);

    return buffer;
}

void createFifo(std::string name, int *inputFd, int *outputFd){

    printf("Creating fifo\n");

    if( (mkfifo(name.c_str(), FILE_MODE) < 0) && (errno != EEXIST) ){
        printf("Can't create the %s fifo\n", name.c_str());
        exit(1);
    }

    printf("Opening Files\n");

    int fd = open(name.c_str(), O_RDWR, 0);

    *inputFd = open(name.c_str(), O_RDONLY, 0);
    *outputFd = open(name.c_str(), O_WRONLY, 0);   /* Never used */

    close(fd);

    printf("DONE\n");
}


void destroyFifo(std::string name, int in, int out){
    close(in);
    close(out);

    remove(name.c_str());
}

void terminalMessage(std::string msg, int type){
    std::cout << msg << std::endl;
}

std::string int_to_str(int v){
    std::ostringstream convert;
    convert << v;
    return convert.str();

}
