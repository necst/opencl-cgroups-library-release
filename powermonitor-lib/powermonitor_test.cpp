/*
****************************** Copyright & License *****************************
OpenCL-CGroups-Library v0.1 is a software controller for OpenCL applications running on
Heterogeneous System Architectures hosting a Linux operating system for optimizing
the performance/Watt metric. Copyright (C) 2016 Politecnico di Milano.

This framework is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This framework is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details 
(http://www.gnu.org/licenses/).

Neither the name of Politecnico di Milano nor the names of its contributors may be 
used to endorse or promote products derived from this software without specific 
prior written permission.
********************************************************************************
*/

#include "powermonitor-lib.h"
#include "utils.h"
#include <fcntl.h>

#include <unistd.h>
#include <iostream>

int main(){

    PowerMonitor powermon(CLIENT_MODE);

    for(int i=1; i<4; i++){

        powermon.startAcquisition();

        sleep(i);

        powermon.stopAcquisition();

        powermon.acquireData();

        std::cout << "Iteration " << i << std::endl;
        std::cout << "\tLittle Power: " << powermon.powerDataBuffer[LITTLE_POWER] << std::endl;
        std::cout << "\tBig Power: " << powermon.powerDataBuffer[BIG_POWER] << std::endl;
        std::cout << "\tGPU Power: " << powermon.powerDataBuffer[GPU_POWER] << std::endl;
        std::cout << "\tMemory Power: " << powermon.powerDataBuffer[MEM_POWER] << std::endl;

    }

}
