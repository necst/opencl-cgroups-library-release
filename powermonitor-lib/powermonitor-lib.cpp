/*
****************************** Copyright & License *****************************
OpenCL-CGroups-Library v0.1 is a software controller for OpenCL applications running on
Heterogeneous System Architectures hosting a Linux operating system for optimizing
the performance/Watt metric. Copyright (C) 2016 Politecnico di Milano.

This framework is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This framework is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details 
(http://www.gnu.org/licenses/).

Neither the name of Politecnico di Milano nor the names of its contributors may be 
used to endorse or promote products derived from this software without specific 
prior written permission.
********************************************************************************
*/


#include "powermonitor-lib.h"
#include <assert.h>
#include "utils.h"
#include <fcntl.h>
#include <unistd.h>
#include <iostream>
#include <string.h>

PowerMonitor::PowerMonitor(){}

PowerMonitor::PowerMonitor(int mode){
    this->mode = mode;

    // Opening Pipes
    if(mode == SERVER_MODE){
        createFifo(NAMED_PIPE_CLIENT2SERVER, &client2server.inputFd, &client2server.outputFd);
        createFifo(NAMED_PIPE_SERVER2CLIENT, &server2client.inputFd, &server2client.outputFd);
    }

    if(mode == CLIENT_MODE){
        client2server.inputFd = open(NAMED_PIPE_CLIENT2SERVER, O_RDONLY);
        client2server.outputFd = open(NAMED_PIPE_CLIENT2SERVER, O_WRONLY);

        server2client.inputFd = open(NAMED_PIPE_SERVER2CLIENT, O_RDONLY);
        server2client.outputFd = open(NAMED_PIPE_SERVER2CLIENT, O_WRONLY);
    }

    assert(client2server.inputFd > 0);
    assert(client2server.outputFd > 0);
    assert(server2client.outputFd > 0);
    assert(server2client.outputFd > 0);
}

void PowerMonitor::startAcquisition(){
    assert(mode == CLIENT_MODE);
    Message m;
    strcpy(m.content.c, START_ACQUISITION);
    int size = write(client2server.outputFd, &m, sizeof(Message));
    assert(size == sizeof(Message));
}

void PowerMonitor::stopAcquisition(){
    assert(mode == CLIENT_MODE);
    Message m;
    strcpy(m.content.c, STOP_ACQUISITION);
    int size = write(client2server.outputFd, &m, sizeof(Message));
    assert(size == sizeof(Message));
}

void PowerMonitor::acquireData(){
    assert(mode == CLIENT_MODE);
    Message m;
    int size = read(server2client.inputFd, &m, sizeof(Message));
    assert(size == sizeof(Message));

//    for(int i=0; i<MESSAGE_SIZE/sizeof(float); i++)
//        std::cout << m.content.f[i] << " - ";
//    std::cout << std::endl;

    memcpy(powerDataBuffer, m.content.f, MESSAGE_SIZE);
}

void PowerMonitor::sendData(){
    assert(mode == SERVER_MODE);
    Message m;
    memcpy(m.content.f, powerDataBuffer, MESSAGE_SIZE);

    for(int i=0; i<MESSAGE_SIZE/sizeof(float); i++)
        std::cout << m.content.f[i] << " - ";

    std::cout << std::endl;

    int size = write(server2client.outputFd, &m, sizeof(Message));
    assert(size == sizeof(Message));
}

Message PowerMonitor::readMessage(){
    assert(mode == SERVER_MODE);
    Message m;
    int size = read(client2server.inputFd, &m, sizeof(Message));
    assert(size == sizeof(Message));
    return m;
}

PowerMonitor::~PowerMonitor(){
    close(server2client.inputFd);
    close(server2client.outputFd);
    close(client2server.inputFd);
    close(server2client.outputFd);
}

float PowerMonitor::getTotalPower(){
    return powerDataBuffer[LITTLE_POWER] + powerDataBuffer[BIG_POWER] + powerDataBuffer[GPU_POWER] + powerDataBuffer[MEM_POWER];
}
