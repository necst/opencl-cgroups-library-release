/*This code has been readapted from an original version downloaded from https://github.com/hardkernel/EnergyMonitor*/

/*
 * This class represents the interface to the virtual file system to get information from the Odroid sensors (current, voltage power. temperature and utilization).
 * This code has been taken (and slightly readapted) from the Odroid EnergyMonitor
 */

#ifndef GETNODE_H
#define GETNODE_H

#include <sys/ioctl.h>
#include <string>
#include <vector>

#define GPUFREQ_NODE    "/sys/devices/11800000.mali/clock"
#define TEMP_NODE       "/sys/devices/10060000.tmu/temp"

typedef struct ina231_iocreg__t {
    char name[20];
    unsigned int enable;
    unsigned int cur_uV;
    unsigned int cur_uA;
    unsigned int cur_uW;
} ina231_iocreg_t;

typedef struct sensor__t {
    int  fd;
    ina231_iocreg_t data;
} sensor_t;

#define INA231_IOCGREG      _IOR('i', 1, ina231_iocreg_t *)
#define INA231_IOCSSTATUS   _IOW('i', 2, ina231_iocreg_t *)
#define INA231_IOCGSTATUS   _IOR('i', 3, ina231_iocreg_t *)

#define DEV_SENSOR_ARM  "/dev/sensor_arm"
#define DEV_SENSOR_MEM  "/dev/sensor_mem"
#define DEV_SENSOR_KFC  "/dev/sensor_kfc"
#define DEV_SENSOR_G3D  "/dev/sensor_g3d"

enum {
    SENSOR_ARM = 0,
    SENSOR_MEM,
    SENSOR_KFC,
    SENSOR_G3D,
    SENSOR_MAX
};

class GetNode {
public:
    GetNode();
    ~GetNode();

    int GetGPUCurFreq();
    int GetCPUCurFreq(int cpuNum);
    int GetCPUTemp(int cpuNum);
    std::vector<int> GetCPUUsage();

    void GetINA231();
    float getLittleA();
    float getLittleV();
    float getLittleW();
    float getBigA();
    float getBigV();
    float getBigW();
    float getGpuA();
    float getGpuV();
    float getGpuW();
    float getMemA();
    float getMemV();
    float getMemW();
    
private:
    std::string cpu_node_list[8];
    float armuV, armuA, armuW; //big
    float g3duV, g3duA, g3duW; //gpu
    float kfcuV, kfcuA, kfcuW; //little
    float memuV, memuA, memuW; //mem
    int usage[8]; //8 cores!

    int open_sensor(const char *node, sensor_t *sensor);
    void close_sensor(sensor_t *sensor);
    void enable_sensor(sensor_t *sensor, unsigned char enable);
    int read_sensor_status(sensor_t *sensor);
    void read_sensor(sensor_t *sensor);

    int calUsage(int cpu_idx, int user, int nice, int system, int idle);

    int OpenINA231();
    void CloseINA231();

    sensor_t sensor[SENSOR_MAX];
    int mOldUserCPU[8];
    int mOldSystemCPU[8];
    int mOldIdleCPU[8];
};

#endif // GETNODE_H
