/*
****************************** Copyright & License *****************************
OpenCL-CGroups-Library v0.1 is a software controller for OpenCL applications running on
Heterogeneous System Architectures hosting a Linux operating system for optimizing
the performance/Watt metric. Copyright (C) 2016 Politecnico di Milano.

This framework is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This framework is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details 
(http://www.gnu.org/licenses/).

Neither the name of Politecnico di Milano nor the names of its contributors may be 
used to endorse or promote products derived from this software without specific 
prior written permission.
********************************************************************************
*/


#ifndef POWERMONITORLIB_H
#define POWERMONITORLIB_H

#include <sys/types.h>

#define NAMED_PIPE_CLIENT2SERVER "/tmp/powermonitor_pipe_client2server"
#define NAMED_PIPE_SERVER2CLIENT "/tmp/powermonitor_pipe_server2client"

#define SERVER_MODE 0
#define CLIENT_MODE 1

#define MESSAGE_SIZE 32

#define START_ACQUISITION "start"
#define STOP_ACQUISITION "stop"

#define LITTLE_POWER 0
#define BIG_POWER 1
#define GPU_POWER 2
#define MEM_POWER 3

typedef union MessageContent{
    char c[MESSAGE_SIZE];
    unsigned int ui[MESSAGE_SIZE/sizeof(unsigned int)];
    int i[MESSAGE_SIZE/sizeof(int)];
    float f[MESSAGE_SIZE/sizeof(float)];
} MessageContent;

typedef struct message{
    pid_t pid;
    MessageContent content;
} Message;


typedef struct pipe{
    int inputFd;
    int outputFd;
} Pipe;

class PowerMonitor{

private:
    Pipe client2server;
    Pipe server2client;
    int mode;

public:

    float powerDataBuffer[MESSAGE_SIZE/sizeof(float)];

    PowerMonitor();
    PowerMonitor(int mode);
    ~PowerMonitor();

    void startAcquisition();
    void stopAcquisition();
    void acquireData();
    void sendData();
    Message readMessage();
    float getTotalPower();
};




#endif // POWERMONITORLIB_H
