/*
****************************** Copyright & License *****************************
OpenCL-CGroups-Library v0.1 is a software controller for OpenCL applications running on
Heterogeneous System Architectures hosting a Linux operating system for optimizing
the performance/Watt metric. Copyright (C) 2016 Politecnico di Milano.

This framework is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This framework is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details 
(http://www.gnu.org/licenses/).

Neither the name of Politecnico di Milano nor the names of its contributors may be 
used to endorse or promote products derived from this software without specific 
prior written permission.
********************************************************************************
*/


#include <iostream>
#include <sys/types.h>
#include <thread>
#include <assert.h>
#include <string.h>
#include <unistd.h>

#include "getNode.h"
#include "powermonitor-lib.h"
#include "utils.h"

#define STOPPED 0
#define RUNNING 1

volatile int status = STOPPED;
volatile unsigned long dataPoints;
PowerMonitor powermon(SERVER_MODE);

volatile unsigned long sampleFrequency = 50000;

void computeAveragePower(){

    GetNode getNode;

    while(true){

        // Get Values

        getNode.GetINA231();

        float littleW = getNode.getLittleW();
        float bigW = getNode.getBigW();
        float gpuW = getNode.getGpuW();
        float memW = getNode.getMemW();

        if(status == RUNNING){
            // Sum if monitoring is active
            powermon.powerDataBuffer[LITTLE_POWER] += littleW;
            powermon.powerDataBuffer[BIG_POWER] += bigW;
            powermon.powerDataBuffer[GPU_POWER] += gpuW;
            powermon.powerDataBuffer[MEM_POWER] += memW;

            dataPoints++;
        }

        usleep(sampleFrequency);
    }

}

int main(int argc, char **argv){
    
    if(argc!=2){
        printf("USAGE: ./powermonitor.exe SAMPLE_TIME\n");
        printf("SAMPLE_TIME: sample frequency in microseconds\n");
        exit(0);
    }
    
    sampleFrequency = atol(argv[1]);

    Message m;

    std::thread sensorsThread (computeAveragePower);

    // Reset acquisition
    memset(powermon.powerDataBuffer, 0, MESSAGE_SIZE/sizeof(float));
    dataPoints = 0;

    while(true){

        m = powermon.readMessage();

        // Send start signal with atomic increment
        if(!strcmp(m.content.c, START_ACQUISITION)){
            assert(status == STOPPED);
            __sync_fetch_and_add(&status, RUNNING);
            std::cout << "Monitor status: " << status << std::endl;
        }

        if(!strcmp(m.content.c, STOP_ACQUISITION)){
            assert(status == RUNNING);

            // Stop with atomic decrement
            __sync_fetch_and_sub(&status, RUNNING);

            std::cout << "Monitor status: " << status << std::endl;

            assert(dataPoints > 0);

            for(int i = 0; i<MESSAGE_SIZE/sizeof(float); i++)
                powermon.powerDataBuffer[i] /= dataPoints;

            // Send data
            powermon.sendData();

            // Reset acquisition
            memset(powermon.powerDataBuffer, 0, MESSAGE_SIZE/sizeof(float));
            dataPoints = 0;
        }

    }

    sensorsThread.join();
}
