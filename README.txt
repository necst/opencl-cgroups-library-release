****************************** Copyright & License *****************************
OpenCL-CGroups-Library v0.1 is a software controller for OpenCL applications running on
Heterogeneous System Architectures hosting a Linux operating system for optimizing
the performance/Watt metric. Copyright (C) 2016 Politecnico di Milano.

This framework is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This framework is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details 
(http://www.gnu.org/licenses/).

Neither the name of Politecnico di Milano nor the names of its contributors may be 
used to endorse or promote products derived from this software without specific 
prior written permission.
********************************************************************************

** Introduction:
OpenCL-Groups-Library v0.1 is a software controller for OpenCL applications running on
Heterogeneous System Architectures hosting a Linux operating system for optimizing
the performance/Watt metric. A description of the implemented controller can be 
found in:
"A Runtime Controller for OpenCL Applications on Heterogeneous System Architectures"
accepted in EWiLi 2016 Workshop (http://syst.univ-brest.fr/ewili2016/) and currently 
under publication in an issue of SIGBED review.

** This folder contains:
_ this "README.txt" file
_ a "make.sh" containing commands to compile the project
_ a "powermonitor-lib" folder containing the power monitoring libraries and application 
  used in the project
_ a "common" folder with common utilities
_ a "opencl-cgroups-lib" containing the source code of the controller
_ an "applications" folder containing a sample application

** Supported Systems
Currently, the controller has been validated on Hardkernel Odroid XU3 loading Ubuntu 15.04.

** How to configure OpenCL runtime environment
_ install ARM Mali OpenCL SDK for enabling OpenCL on the GPU. It can be downloaded from 
  http://malideveloper.arm.com/resources/sdks/mali-opencl-sdk
_ install pocl for enabling OpenCL on the CPU. It can be downloaded from 
  http://portablecl.org
_ create a mali.icd file within the /etc/OpenCL/vendors folder (create the folder if
  necessary) containing the path and the name of OpenCL shared library of ARM Mali OpenCL 
  SDK. The content of the file should be: /usr/lib/arm-linux-gnueabihf/libOpenCL.so
_ create a pocl.icd file within the /etc/OpenCL/vendors folder containing the path and
  the name of OpenCL shared library of pocl. The path depends on the directory the library
  has bee installed into. Generally, the file is located in /usr/local/lib/libOpenCL.so
_ install the OpenCL ICD loader that can be downloaded from https://forge.imag.fr/projects/ocl-icd
  (It is necessary to specify std=c++11 in the Makefile)
_ when compiling an OpenCL application, in order to use the ICD loader it is necessary to
  specify the path of the location the corresponding OpenCL shared library is located.

** How to compile and run OpenCL—CGroups-Library
_ open applications/common.mk file and update the LIBPATH variable to contain the location 
  of libOpenCL.so folder within the ICD loader folder (it is located in the .libs subfolder)
_ ensure that make.sh has executable permissions: chmod +x make.sh
_ ensure that applications/compileAll.sh has executable permissions: chmod +x applications/compileAll.sh
_ just type "./make.sh" from the root of this folder
_ run the power monitor application specifying a sample time in microseconds, e.g. for sampling every 10ms: ./powermonitor-lib/powerMonitor.exe 10000
_ run the test application within its own folder by specifying the number of iterations, 
  e.g. for 100 iterations: ./appl1.exe 100
_ DO NOTE: both application and the power monitor have to be launched with root permissions
  since it is necessary to access power sensors and to control cgroups


** To signal bugs or for questions, please write to: antonio <dot> miele <at> polimi <dot> it
