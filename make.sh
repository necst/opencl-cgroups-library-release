# Compiling common libraries
cd common
make
cd ..

# Compiling powermonitor-lib and application
cd powermonitor-lib
make
cd ..

# Compiling opencl-cgroups-lib
cd opencl-cgroups-lib
make
cd ..

# Compiling applications
cd applications
./compileAll.sh
cd ..
